package com.mehmetsalihbindak.survivalbird;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;

import java.util.Random;

import javax.xml.soap.Text;

public class SurvivalBird extends ApplicationAdapter {
	SpriteBatch batch;
	// Objects Inıtialze
	Texture background;
	Texture bird;
	Texture enemy,enemy2;


	Random random;


	private Music music;
	// widths and heights
	private int pWidth = 0; // Phone
	private int pHeight = 0;
    private int bWidth = 0; // Bird
    private int bHeight = 0;


    private int birdX = 0;
    private int birdY = 0;

    private int enemyX = 0;
    private int enemyY = 0;

    private int enemy2X =0;
    private int enemy2Y =0;

	//Game states 0 for begin, 1 for start, 2 for end
    private int gameState = 0;

	//Game mechanics
    private float velocity = 0;
    private float acceleration = 0;

    private float enemyVelocity = 0;
    private float enemy2Velocity = 0;

    private Circle birdCircle;

    private Circle enemyCircle;
    private Circle enemy2Circle;

    private int score = 0;

    private BitmapFont font,font2,font3;


    //private ShapeRenderer shapeRenderer;


	// Oncreate ile aynı. Oyun başladığında olacak şeyler burada olacak.
	@Override
	public void create () {
		batch = new SpriteBatch();
		music = Gdx.audio.newMusic(Gdx.files.internal("bgmusic.mp3"));
		music.setLooping(true);
		music.setVolume(0.1f);
		music.play();
		// Objects assignment
		background = new Texture("background.png"); // Assign to background image.
		bird = new Texture("bird.png");

		enemy = new Texture("enemy.png");
        enemy2 = new Texture("enemy.png");




        // Easy way to get phone's width and height
		pWidth = Gdx.graphics.getWidth();
		pHeight = Gdx.graphics.getHeight();

		// Bird's height and width
		bWidth = bird.getWidth();
		bHeight = bird.getHeight();


		birdCircle = new Circle();
		enemyCircle = new Circle();
        enemy2Circle = new Circle();

        //shapeRenderer = new ShapeRenderer();

        font = new BitmapFont();
        font.setColor(Color.WHITE);
        font.getData().setScale(5);

        font2 = new BitmapFont();
        font2.setColor(Color.RED);
        font2.getData().setScale(10);

        font3 = new BitmapFont();
        font.setColor(Color.WHITE);
        font3.getData().setScale(6);



		// Bird's coordinate we will change it
		birdX = pWidth/3-bHeight/5;
		birdY = pHeight/2 -bWidth/5;



		//Game Mechanics
		velocity = 0;
		acceleration = 0.5f;

		random = new Random();

        enemyX = (pWidth/4)*3;
        enemyY = birdY;

        enemy2X = pWidth;
        enemy2Y = random.nextInt(pHeight);







	}

	// Oyun devam ettiği sürece olacak şeyler
	@Override
	public void render () {

    batch.begin();

    batch.draw(background,0,0,pWidth,pHeight); // Draw the background that we initialize at create(). x ,y => Phone like cooridinate system.



        if(gameState == 1){

            if(enemyX <= 0 || enemy2X <= 0){
                score++;
                //System.out.println(score); // For Debug
            }



        if(enemyX > 0){
            enemyVelocity = 15;
            enemyX-=enemyVelocity;
        }else {
            enemyX = pWidth;
            enemyY = birdY;
        }

        if(enemy2X > 0){
            enemy2Velocity = random.nextInt(15)+5;
            enemy2X-=enemy2Velocity;
        }else{
            enemy2X = pWidth;
            enemy2Y = random.nextInt(pHeight);
        }



        if(Gdx.input.justTouched()){
         velocity = -10;
        }



		// Game continue conditions
		if(birdY > 0){
			velocity += acceleration;
			birdY -= velocity;
		}else{
	         gameState = 2;
        }

        }else if(gameState == 0){
            if(Gdx.input.justTouched()){
                gameState = 1;
            }

        }else if(gameState == 2){

            font2.draw(batch,"GAME OVER",pWidth/5,(pHeight/3)*2);
            font3.draw(batch,"Tap to play again",pWidth/3,pHeight/2-200);

            if(Gdx.input.justTouched()){
                gameState = 1;
                birdY = pHeight/2 -bWidth/5;
                birdX = pWidth/3-bHeight/5;
                enemyX = pWidth;
                enemy2X = pWidth;
                velocity = 0;

                score = 0;

            }
        }

		batch.draw(bird,birdX,birdY ,pWidth/15,pHeight/10);

		batch.draw(enemy,enemyX,enemyY,pWidth/15,pHeight/10);
        batch.draw(enemy2,enemy2X,enemy2Y,pWidth/15,pHeight/10);

        font.draw(batch,String.valueOf(score),100,200);
		batch.end();

		birdCircle.set(birdX + pWidth/30,birdY+pHeight/20, pWidth/30 - birdCircle.x/25);
		enemyCircle.set(enemyX + pWidth/30,enemyY+pHeight/20,pWidth/30 - birdCircle.x/25);
        enemy2Circle.set(enemy2X + pWidth/30,enemy2Y+pHeight/20,pWidth/30 - birdCircle.x/25);



		//shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
		//shapeRenderer.setColor(Color.BLACK);
		//shapeRenderer.circle(birdCircle.x,birdCircle.y,birdCircle.radius);
		//shapeRenderer.circle(enemyCircle.x,enemyCircle.y,enemyCircle.radius);
		//shapeRenderer.circle(enemy2Circle.x,enemy2Circle.y,enemy2Circle.radius);

        if(Intersector.overlaps(birdCircle,enemyCircle) || Intersector.overlaps(birdCircle,enemy2Circle)){
            gameState = 2;
        }

        //shapeRenderer.end();

	}
	
	@Override
	public void dispose () {
        music.dispose();
	}
}
