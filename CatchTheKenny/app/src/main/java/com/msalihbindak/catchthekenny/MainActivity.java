package com.msalihbindak.catchthekenny;

import android.media.Image;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView textTime;
    TextView textScore;

    ImageView keny1;
    ImageView keny2;
    ImageView keny3;
    ImageView keny4;
    ImageView keny5;
    ImageView keny6;
    ImageView keny7;
    ImageView keny8;
    ImageView keny9;
    int score;

    ImageView[] imageArray;

    Handler handler;
    Runnable runnable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        keny1 = findViewById(R.id.imageView1);
        keny2 = findViewById(R.id.imageView2);
        keny3 = findViewById(R.id.imageView3);
        keny4 = findViewById(R.id.imageView4);
        keny5 = findViewById(R.id.imageView5);
        keny6 = findViewById(R.id.imageView6);
        keny7 = findViewById(R.id.imageView7);
        keny8 = findViewById(R.id.imageView8);
        keny9 = findViewById(R.id.imageView9);

        imageArray = new ImageView[] {keny1,keny2,keny3,keny4,keny5,keny6,keny7,keny8,keny9};


        hideImages();

        score = 0;

        new CountDownTimer(30000,  1000){

            //textTime = findViewById(R.id.textTime);
            @Override
            public void onTick(long l) {

                textTime = findViewById(R.id.textTime);
                textTime.setText("Time: "+ l/1000);
            }

            @Override
            public void onFinish() {
                textTime = findViewById(R.id.textTime);
                textTime.setText("Time is up!");
                for (ImageView image:imageArray){
                    image.setVisibility(View.INVISIBLE);
                }
                handler.removeCallbacks(runnable);
            }


        }.start();

    }


    public void increaseScore(View view){

        textScore = findViewById(R.id.textScore);

        score++;
        textScore.setText("Score: "+score);


    }

    public void hideImages(){

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

                for (ImageView image:imageArray){
                    image.setVisibility(View.INVISIBLE);
                }
                Random r = new Random();
                int i = r.nextInt(8-0);
                imageArray[i].setVisibility(View.VISIBLE);

                handler.postDelayed(this,500);
            }
        };

        handler.post(runnable);
    }







}
