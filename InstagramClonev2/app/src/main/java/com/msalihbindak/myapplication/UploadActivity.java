package com.msalihbindak.myapplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;

public class UploadActivity extends AppCompatActivity {
    EditText comment;
    ImageView image;
    Bitmap choosenImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        comment = findViewById(R.id.commentText);
        image = findViewById(R.id.imageView);
    }

    public void sendPhoto(View view){

            String commentText = comment.getText().toString();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    choosenImage.compress(Bitmap.CompressFormat.PNG,50,byteArrayOutputStream);
            byte[] bytes = byteArrayOutputStream.toByteArray();


            ParseFile parseFile = new ParseFile("iamge.png",bytes);

            ParseObject object = new ParseObject("Posts");
        object.put("image",parseFile);
        object.put("comment",commentText);
        object.put("username", ParseUser.getCurrentUser().getUsername());
        object.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e != null){
                        Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(),"Your Post Successfully Shared",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(),FeedActivity.class);
                        startActivity(intent);
                    }
                }
        });



    }

    public void selectPhoto(View view){
        // İznim var mı?
        if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            //İznim olmadığı için istemeye gidiyorum
            requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},1);
        }else{
            //İznim olduğu için resmimi çekmeye gidiyorum....
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent,2); // onRequestPermissionResult'a gidiyor
        }
    }

    // İznim yok ama izin istedim, sonucu ne olacak...
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 1){
        //  iznimi istemeye gittiğimi teyit ediyorum
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //Eleman izni vermis. Resmi çekmeye gidiyorum...
                // CheckSelfPermission metodundaki else'nin aynısı çünkü orda izin almışım...
                Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent,1);
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    // Hocam resmi aldım geldim şimdi ne yapıyoruz?
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == 2 && resultCode == RESULT_OK && data != null){
            // İznin istediğimi, OK cevabı döndüğünü ve resmi galeriden çekebildiğimi teyit ettim.
            Uri selected = data.getData();
            try {
                choosenImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(),selected);
                image.setImageBitmap(choosenImage);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }






}
