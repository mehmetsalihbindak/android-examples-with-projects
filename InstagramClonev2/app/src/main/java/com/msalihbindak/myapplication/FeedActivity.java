package com.msalihbindak.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class FeedActivity extends AppCompatActivity {


    ArrayList<String> usernameFromParse;
    ArrayList<Bitmap> userImageFromParse;
    ArrayList<String> userCommentFromParse;
    ListView listView;
    PostClass adapter;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_post,menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.add_post){
            Intent goUpload = new Intent(getApplicationContext(),UploadActivity.class);
            startActivity(goUpload);
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);


        usernameFromParse    = new ArrayList<String>();
        userImageFromParse   = new ArrayList<Bitmap>();
        userCommentFromParse = new ArrayList<String>();

        adapter = new PostClass(usernameFromParse,userImageFromParse,userCommentFromParse,this);


        listView = findViewById(R.id.listView);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(getApplicationContext(),"Clicked" + usernameFromParse.get(position),Toast.LENGTH_LONG).show();

            }
        });

        getParseData();


    }


    public void getParseData(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Posts");
        // Bu noktaya ekleyeceğimiz filtrelerle kullanıcının hangi resimleri görebileceğini ayarlayabiliriz...
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {

                if(e == null){
                    if(objects.size()>0){

                        for(final ParseObject object : objects){

                            ParseFile parseFile = (ParseFile) object.get("image");

                            parseFile.getDataInBackground(new GetDataCallback() {
                                @Override
                                public void done(byte[] data, ParseException e) {
                                    if(e == null && data != null){

                                        Bitmap bitmap = BitmapFactory.decodeByteArray(data,0,data.length);

                                        userImageFromParse.add(bitmap);
                                        usernameFromParse.add(object.getString("username"));
                                        userCommentFromParse.add(object.getString("comment"));

                                        adapter.notifyDataSetChanged();

                                    }
                                }
                            });



                        }

                    }

                }else{
                    Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

}
