package com.msalihbindak.kentsimgeleriv1;

import android.graphics.Bitmap;

import javax.microedition.khronos.opengles.GL;

/**
 * Created by salih on 18.02.2018.
 */

public class Globals {
    private static Globals instance;
    private Bitmap chosenImage;

    private Globals(){

    }


    public void setChosenImage(Bitmap chosenImage){
        this.chosenImage = chosenImage;
    }

    public  Bitmap getChosenImage(){
        return this.chosenImage;
    }

    public static Globals getInstance(){
        if(instance == null){
            instance = new Globals();
        }

        return instance;

    }


}
