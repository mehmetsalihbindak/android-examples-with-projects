package com.msalihbindak.myapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        SharedPreferences sharedPreferences = this.getSharedPreferences("com.msalihbindak.myapplication", Context.MODE_PRIVATE);

        int age = 40;

        //sharedPreferences.edit().putInt("userAge",age).apply();

        int savedAge = sharedPreferences.getInt("userAge",0);
        // getInt ile hafızada değer aramaya gönderiyoruz w/ userAge etiketli olan bir değer...
        // eğer değeri bulamazsa döndüreceği değer -> 0 olacak.

        System.out.println("Saved value: "+savedAge);

        age = 41;

        sharedPreferences.edit().putInt("userAge",age).apply();   // Editlemek için

        int savedAge2 = sharedPreferences.getInt("userAge",0);

        System.out.println("Saved Value"+savedAge2);

        sharedPreferences.edit().remove("userAge").apply(); // Silmek için.



    }
}
