package com.msalihbindak.kenyv1;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView textTime;
    TextView textScore;
    ImageView imageView1;
    ImageView imageView2;
    ImageView imageView3;
    ImageView imageView4;
    ImageView imageView5;
    ImageView imageView6;
    ImageView imageView7;
    ImageView imageView8;
    ImageView imageView9;
    ImageView[] imageArray;

    Handler handler;
    Runnable runnable;

    int score; // Skoru arttırma durumunda skorun tutulacağı değişken.


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView1 = findViewById(R.id.imageView1);
        imageView2 = findViewById(R.id.imageView2);
        imageView3 = findViewById(R.id.imageView3);
        imageView4 = findViewById(R.id.imageView4);
        imageView5 = findViewById(R.id.imageView5);
        imageView6 = findViewById(R.id.imageView6);
        imageView7 = findViewById(R.id.imageView7);
        imageView8 = findViewById(R.id.imageView8);
        imageView9 = findViewById(R.id.imageView9);



        imageArray = new ImageView[]{imageView1,imageView2,imageView3,imageView4,imageView5,imageView6,imageView7,imageView8,imageView9};

        score = 0;


        new CountDownTimer(10000,1000) {
            @Override
            public void onTick(long l) {
                textTime = findViewById(R.id.textTime);
                textTime.setText("Time: " + l/1000 ); // Kaç saniye kaldığını gösterecek
            }

            @Override
            public void onFinish() {
                textTime = findViewById(R.id.textTime);
                textTime.setTextColor(Color.RED);
                textTime.setText("Game Over");
                handler.removeCallbacks(runnable);
                for (ImageView image:imageArray){
                    image.setVisibility(View.INVISIBLE);
                }

            }
        }.start();


        hideImages();




    }

    public void increaseScore(View view){
        textScore = findViewById(R.id.textScore);
        score++;
        textScore.setText("Score: "+score);
    }

    public void hideImages(){

        handler  = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                for (ImageView image:imageArray){
                    image.setVisibility(View.INVISIBLE);
                }

                Random r = new Random();
                int i = r.nextInt(8-0);

                imageArray[i].setVisibility(View.VISIBLE);

                handler.postDelayed(this,500);

            }
        };

        handler.post(runnable);

    }


}
