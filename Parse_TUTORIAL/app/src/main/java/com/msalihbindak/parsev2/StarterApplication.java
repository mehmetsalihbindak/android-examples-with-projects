package com.msalihbindak.parsev2;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

/**
 * Created by salih on 20.03.2018.
 */

public class StarterApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        //Enable Local Datastore
        Parse.enableLocalDatastore(this);

        //Initialization code
        Parse.initialize(this);

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        //Optionally enable public read access
        defaultACL.setPublicWriteAccess(true);
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL,true);



    }
}
