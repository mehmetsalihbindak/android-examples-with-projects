package com.msalihbindak.parsev2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ParseAnalytics.trackAppOpenedInBackground(getIntent());
        textView = findViewById(R.id.textView);

        // Obje Oluşturma -> Tablo oluşturma
/*
        ParseObject object = new ParseObject("Fruits");
        object.put("name","armut");  // Veri girme  put(KEY,VALUE) KEY -> COLUMN NAME , VALUE -> VALUE
        object.put("calories",1000);

        object.saveInBackground(new SaveCallback() {   // Oluşturduğumuz objeleri db ye push ediyoruz.
            @Override
            public void done(ParseException e) {
                if(e != null){
                    e.printStackTrace();               // Hatayı yazdır
                }else{
                    System.out.println("Object Created!"); // Olay başarıyla bittikten sonra yazılacak mesaj
                }
            }
        });
*/
/*
// https://stackoverflow.com/questions/13251955/how-can-i-update-current-object-in-parse-com-with-javascript
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Fruits");
         ParseObject x = new ParseObject("Fruits");

        try {
            x = query.get("jkIxObiQIB");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        x.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null){
                    x.add("name","banana");
                }else{
                    System.out.println(e.getLocalizedMessage());
                }


            }
        });

        // END OBJE OLUŞTURMA
*/

        // ID ile veri update işlemi, PetQR için en uygun yapı bu ...
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Fruits");

        query.getInBackground("jkIxObiQIB", new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                object.put("name","banana");
                object.saveInBackground();
            }
        });




/*
        //Condition'a bağlı olarak veri çekme, ID bilmiyorsak -> Bu da admin panelinde kullanılabilir...

        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Fruits");
        parseQuery.whereGreaterThan("calories",120);  // WHERE
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null && objects.size()>0){

                    for(ParseObject object : objects){
                        textView.setText(object.getString("name"));
                    }
                    // SELECT name,calories FROM Fruits WHERE calories > 120

                }else{
                    e.printStackTrace();
                }
            }
        });



*/
    }
}
