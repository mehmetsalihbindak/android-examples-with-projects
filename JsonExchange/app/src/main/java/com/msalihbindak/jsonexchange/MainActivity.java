package com.msalihbindak.jsonexchange;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    TextView textView9;
    TextView textView10;
    TextView textView11;
    EditText editText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        textView9 = findViewById(R.id.textView9);
        textView10 = findViewById(R.id.textView10);
        textView11 = findViewById(R.id.textView11);
        editText = findViewById(R.id.editText);




    }


    public void getRates(View view){
        DownloadData downloadData = new DownloadData();


        try {
            String base = editText.getText().toString();
            String url = "https://api.fixer.io/latest?base="+base;
            downloadData.execute(url);

        }catch (Exception e){
            e.printStackTrace();
        }
    }



        private class DownloadData extends AsyncTask<String,Void,String>{


            @Override
            protected String doInBackground(String... strings) {

                String result = "";
                URL url;
                HttpURLConnection httpURLConnection;

                try{
                    // URLye bağlan +  data çekme
                    url = new URL(strings[0]);
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    int data = inputStreamReader.read();

                    while (data > 0){
                        char character =  (char) data;
                        result += character;
                        data=inputStreamReader.read();
                    }

                    return  result;
                }catch (Exception e){
                    e.printStackTrace();
                    return null;
                }
                // END URLye bağlan + data çekme

            }


            @Override
            protected void onPostExecute(String s) {

                //System.out.println("My data is: "+s); // Uygulamamız çalışıyor mu diye çektiğimiz veriyi yazdırdık. artık ihtiyaç yok. direk işlemeye geçiyoruz.
                super.onPostExecute(s);
                try{

                    JSONObject jsonObject = new JSONObject(s);
                    String rates = jsonObject.getString("rates");
                    JSONObject jsonObject1 = new JSONObject(rates);
                    String turk = jsonObject1.getString("TRY");
                    String czk = jsonObject1.getString("CZK");
                    String dkk = jsonObject1.getString("DKK");
                    textView9.setText(turk);
                    textView10.setText(czk);
                    textView11.setText(dkk);

                }catch (Exception e){
                    e.printStackTrace();
                }


            }


    }





}
