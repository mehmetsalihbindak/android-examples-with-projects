package com.msalihbindak.runnable;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    TextView timer;
    int sayac;
    Handler handler;
    Runnable run;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);






    }

    public void start(View view){

        timer = findViewById(R.id.textView);

        sayac = 0;

        handler = new Handler();
        run     = new Runnable() {
            @Override
            public void run() {
                timer.setText("Timer "+sayac);
                sayac++;
                timer.setText("Timer "+sayac);
                handler.postDelayed(this,100); // neden this? Çünkü run içindeyiz ve yine run olsun istiyoruz. o yüzden this olur. caizdir.
            }
        };

        handler.post(run);



    }

    public void stop(View view){

        handler.removeCallbacks(run);

        sayac = 0;

        timer = findViewById(R.id.textView);

        timer.setText("Timer "+sayac);


    }


}
