package com.msalihbindak.instagramclone;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.List;

public class MainActivity extends AppCompatActivity {


    EditText usernameText;
    EditText passwordText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        usernameText = findViewById(R.id.textUserName);
        passwordText = findViewById(R.id.textPassword);

        /*
        // Obje oluşturma, database e kaydetme, veri ekleme işleri
        ParseObject object = new ParseObject("Fruits"); // Database içinde tablo oluşturuyoruz aslında
        object.put("name","banana");
        object.put("calories",200);
        object.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e != null){
                    e.printStackTrace();
                }else
                {
                    System.out.println("--->Created Fruit.");
                }
            }
        });
        */

        /*
        // Query yollama ve veri çekme işlemleri...

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Fruits"); // SELECT TABLE...
        query.getInBackground("zYB9G680BL", new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if(e == null && object != null){
                    System.out.println("Fruit Name: "+object.get("name").toString().toUpperCase());
                    System.out.println("Fruit Calories: "+object.get("calories").toString());
                }else{
                    e.printStackTrace();
                }
            }
        });
        */

        /*
        // Koşula bağlı veri çekmeee
        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Fruits");
        parseQuery.whereGreaterThan("calories",190);  // Equals falan da kullanabiliriz. duruma göre....
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null){
                    if(objects.size()> 0){

                        for(ParseObject object : objects){
                            System.out.println("Fruit: "+object.get("name").toString().toUpperCase()    );
                            System.out.println("Calories: "+object.get("calories").toString());
                        }

                    }

                }else{
                    e.printStackTrace();
                }
            }
        });
        */


    }

    public void singup(View view){

        ParseUser user = new ParseUser();
        user.setUsername(usernameText.getText().toString());
        user.setPassword(passwordText.getText().toString());

        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null){
                    Toast.makeText(getApplicationContext(),"User Created Succesfully",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(),FeedActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),e.getLocalizedMessage().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    public void login(View view){
       ParseUser.logInInBackground(usernameText.getText().toString(), passwordText.getText().toString(), new LogInCallback() {
           @Override
           public void done(ParseUser user, ParseException e) {
               if (e == null) {
                   if(user != null){
                       Toast.makeText(getApplicationContext(),"Welcome "+user.getUsername().toString(),Toast.LENGTH_LONG).show();
                       Intent intent = new Intent(getApplicationContext(),FeedActivity.class);
                       startActivity(intent);
                   }

               }else{
                   Toast.makeText(getApplicationContext(),e.getLocalizedMessage().toString(),Toast.LENGTH_LONG).show();
               }
           }
       });
    }




}
