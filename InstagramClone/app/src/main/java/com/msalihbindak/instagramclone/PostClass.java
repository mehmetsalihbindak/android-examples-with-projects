package com.msalihbindak.instagramclone;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by salih on 11.03.2018.
 */

public class PostClass extends ArrayAdapter<String> {

    private final ArrayList<String> username;
    private final ArrayList<Bitmap> userImage;
    private final ArrayList<String> userComment;
    private final Activity context;


    public PostClass(ArrayList<String> username, ArrayList<Bitmap> userImage, ArrayList<String> userComment, Activity context) {
        super(context,R.layout.custom_view,username);
        this.username = username;
        this.userImage = userImage;
        this.userComment = userComment;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        LayoutInflater layoutInflater = context.getLayoutInflater();
        View customView = layoutInflater.inflate(R.layout.custom_view,null,true);
        TextView usernameText = customView.findViewById(R.id.username);
        ImageView imageView = customView.findViewById(R.id.userImage);
        TextView commentText = customView.findViewById(R.id.userComment);

        usernameText.setText(username.get(position));
        imageView.setImageBitmap(userImage.get(position));
        commentText.setText(userComment.get(position));


        return customView;
    }
}
