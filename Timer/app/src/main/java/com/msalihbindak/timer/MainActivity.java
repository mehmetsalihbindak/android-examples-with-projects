package com.msalihbindak.timer;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new CountDownTimer(10000, 1000) {


            @Override
            public void onTick(long milisUntilFinished) {

                TextView textView = findViewById(R.id.textView);

                textView.setText("Left"+ milisUntilFinished/1000); // ms cinsinden oldugu için 1000 x yapıyoruz.

            }

            @Override
            public void onFinish() {

                Toast.makeText(getApplicationContext(),"ZAMAN DOLDU",Toast.LENGTH_LONG).show();
                TextView textView = findViewById(R.id.textView);

                textView.setText("Left"+ 0);


            }


        }.start();
    }
}