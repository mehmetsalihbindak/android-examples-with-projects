package com.msalihbindak.basicchronometer;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {


    TextView time;
    int number = 0;
    Handler handler;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    public void start(View view){
        time = findViewById(R.id.textTimer);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                time.setText(String.valueOf(number));
                number++;
                time.setText(String.valueOf(number));
                handler.postDelayed(this,1000);
            }
        };

        handler.post(runnable);


    }

    public void stop(View view){

        handler.removeCallbacks(runnable);
        number=0;
        time.setText(String.valueOf(number));

    }


    public void pause(View view){

        handler.removeCallbacks(runnable);
        //number=0;
        time.setText(String.valueOf(number));

    }

}
